package org.emerjoin.mpesa4j.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.emerjoin.mpesa4j.Transaction;
import org.emerjoin.mpesa4j.exception.service.InfrastructureException;
import org.emerjoin.mpesa4j.exception.service.ProtocolViolationException;
import org.emerjoin.mpesa4j.exception.service.response.BadTokenException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Mario Junior.
 */
public class Response {

    private ResponseCode code;
    private String description;
    private Map<String,String> params = new HashMap<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(Response.class);

    private static final String KEY_RESPONSE_CODE = "output_ResponseCode";
    private static final String KEY_RESPONSE_DESC = "output_ResponseDesc";

    private Response(Map<String,String> map){
        if(!map.containsKey(KEY_RESPONSE_CODE)||!map.containsKey(KEY_RESPONSE_DESC))
            throw new IllegalArgumentException("response code and description must be present in the map");
        String responseCode = map.get(KEY_RESPONSE_CODE);
        Optional<ResponseCode> responseCodeOptional = getResponseCode(responseCode);
        if(!responseCodeOptional.isPresent())
            throw new ProtocolViolationException("Unknown response code: "+responseCode);
        this.code = responseCodeOptional.get();
        this.description = map.get(KEY_RESPONSE_DESC);
        this.params.putAll(map);
        this.params.remove(KEY_RESPONSE_CODE);
        this.params.remove(KEY_RESPONSE_DESC);
    }

    public static Response from(Map<String,String> map){
        if(map==null||map.isEmpty())
            throw new IllegalArgumentException("map must not be null nor empty");
        return new Response(map);
    }

    public static Response parse(CloseableHttpResponse response){
        try {
            Header contentTypeHeader = response.getFirstHeader("Content-Type");
            if(!Constants.APPLICATION_JSON_MIME.equalsIgnoreCase(contentTypeHeader.getValue()))
                throw new ProtocolViolationException("Unexpected Content-Type: "+contentTypeHeader.getValue());
            LOGGER.debug("Response status line: "+response.getStatusLine().toString());
            String responseJson = IOUtils.toString(response.getEntity().getContent(), "utf-8");
            LOGGER.debug("Response Content: "+responseJson);
            ObjectMapper mapper = new ObjectMapper();
            Map<String, String> map = mapper.readValue(responseJson, new TypeReference<Map>() {});
            String outputError = map.get("output_error");
            if(outputError!=null&&!outputError.isEmpty()){
                if("Bad API Key".equalsIgnoreCase(outputError)){
                    throw new BadTokenException(outputError);
                }
            }
            return from(map);
        }catch (IOException ex){
            throw new InfrastructureException("error parsing response content",
                    ex);
        }
    }


    public String getDescription(){

        return description;

    }

    private Optional<ResponseCode> getResponseCode(String code){
        for(ResponseCode responseCode: ResponseCode.values()){
            if(responseCode.match(code))
                return Optional.of(responseCode);
        }
        return Optional.empty();
    }

    public ResponseCode getCode(){

        return code;

    }


    public boolean hasParam(String name){
        if(name==null||name.isEmpty())
            throw new IllegalArgumentException("name must not be null nor empty");
        return this.params.containsKey(
                name);
    }

    public String getParam(String name){

        return getParam(name,null);

    }

    public String getParam(String name, String def){
        if(name==null||name.isEmpty())
            throw new IllegalArgumentException("name must not be null nor empty");
        String value = params.get(name);
        if(value==null&&def!=null)
            return def;
        return value;
    }

    @Override
    public String toString() {
        return String.format("Response[code=%s,desc=%s,params=%s]",
                code,description,
                params);
    }

}
