package org.emerjoin.mpesa4j.impl;

import org.emerjoin.mpesa4j.Configuration;
import org.emerjoin.mpesa4j.MpesaOperation;

public class RequestContext {

    private MpesaOperation operation;
    private Configuration configuration;

    RequestContext(MpesaOperation operation, Configuration configuration){
        this.operation = operation;
        this.configuration = configuration;
    }

    MpesaOperation getOperation() {
        return operation;
    }

    Configuration getConfiguration() {
        return configuration;
    }

    boolean isOperationOfType(Class<? extends MpesaOperation> type){
        return type.isInstance(operation);
    }

}
