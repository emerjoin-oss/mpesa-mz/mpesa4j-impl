package org.emerjoin.mpesa4j.impl;

import org.emerjoin.mpesa4j.MpesaOperation;
import org.emerjoin.mpesa4j.Ref;

public class GetTransactionStatus implements MpesaOperation {

    private Ref transactionRef;

    GetTransactionStatus(Ref transactionRef){
        this.transactionRef = transactionRef;
    }

    public Ref getTransactionRef() {
        return transactionRef;
    }
}
