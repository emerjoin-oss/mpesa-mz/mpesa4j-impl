package org.emerjoin.mpesa4j.impl;

import org.emerjoin.mpesa4j.*;
import org.emerjoin.mpesa4j.c2b.*;
import org.emerjoin.mpesa4j.exception.MpesaServiceException;
import org.emerjoin.mpesa4j.exception.service.ProtocolViolationException;
import org.emerjoin.mpesa4j.exception.service.response.*;
import org.emerjoin.mpesa4j.exception.service.response.business.*;
import org.emerjoin.mpesa4j.exception.service.response.config.InvalidInitiatorIdentifierException;
import org.emerjoin.mpesa4j.exception.service.response.config.InvalidShortCodeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import java.nio.InvalidMarkException;
import java.util.Optional;

/**
 * @author Mario Junior.
 */
@ApplicationScoped
public class MpesaServiceImpl implements MpesaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MpesaServiceImpl.class);

    private RequestContext createRequestContext(MpesaOperation operation){
        Configuration configuration = Configuration.current().clone();
        return new RequestContext(operation,
                configuration);
    }

    private RequestBuilder buildRequest(String path,RequestContext context){

        return new RequestBuilder(path, context.
                getConfiguration());

    }

    private boolean isC2bPaymentRequest(Transaction transaction){
        return transaction instanceof C2BPaymentRequest;
    }

    private void commonStatusCheck(Response response, RequestContext context){
        switch (response.getCode()){
            case INTERNAL_ERROR:
                throw new MpesaInternalException();
            case REQUEST_TIMEOUT:
                if(context.isOperationOfType(C2BPaymentRequest.class))
                    throw new PaymentApprovalTimeoutException();
                else throw new RequestTimeoutException();
            case INVALID_SECURITY_CREDENTIAL:
                throw new InvalidSecurityCredentialException();
            case INITIATOR_AUTHENTICATION_ERROR:
                throw new InitiatorAuthenticationErrorException();
            case TEMPORARY_OVERLOADING:
                throw new TemporaryOverloadingException();
            case NOT_ALL_PARAMETERS_PROVIDED:
                throw new RequiredParamsMissingException();
            case INVALID_TRANSACTION_REFERENCE:
                if(context.isOperationOfType(Transaction.class))
                    throw new InvalidTransactionRefException(((Transaction) context.getOperation()).getTransactionRef());
                if(context.isOperationOfType(GetTransactionStatus.class))
                    throw new InvalidTransactionRefException(((GetTransactionStatus) context.getOperation()).getTransactionRef());
            case INVALID_TRANSACTION_ID:
                throw new InvalidTransactionIdException();
            case INVALID_3RDPARTY_REF:
                if(context.isOperationOfType(C2BPaymentRequest.class))
                    throw new InvalidPaymentSubjectException(((C2BPaymentRequest) context.getOperation()).getTransactionRef().toString());
                throw new Invalid3rdPartyReferenceException();
            case INVALID_REFERENCE_USED:
                throw new InvalidReferenceUsedException();
            case INVALID_INITIATOR_IDENTIFIER:
                throw new InvalidInitiatorIdentifierException(context.getConfiguration().getInitiatorIdentifier());
            case INVALID_OPERATION_TYPE:
                throw new InvalidOperationTypeException();
            case INVALID_MARKET:
                throw new InvalidMarkException();
            case INVALID_SHORTCODE_USED:
                throw new InvalidShortCodeException(context.getConfiguration().getServiceProviderCode());
        }
    }


    private void paymentStatusCheck(Response response, RequestContext context){
        ResponseCode responseCode = response.getCode();
        switch (responseCode){
            case TRANSACTION_FAILED:
                throw new TransactionFailedException();
            case INVALID_AMOUNT_USED:
                if(context.isOperationOfType(C2BPaymentRequest.class))
                    throw new InvalidTransactionAmountException(((C2BPaymentRequest) context.getOperation()).getAmount()
                            .value());
                if(context.isOperationOfType(PaymentReverse.class)){
                    PaymentReverse paymentReverse = (PaymentReverse) context.getOperation();
                    Optional<Amount> partialAmount = paymentReverse.getPartialAmount();
                    if(partialAmount.isPresent())
                        throw new InvalidTransactionAmountException(partialAmount.get().
                                value());
                }
            case INVALID_MSISDN:
                if(context.isOperationOfType(C2BPaymentRequest.class)){
                    C2BPaymentRequest c2BPaymentRequest = (C2BPaymentRequest) context.getOperation();
                    throw new InvalidCustomerMsisdnException(c2BPaymentRequest.
                            getMobileNumber().toString());
                }
            case TRANSACTION_CANCELLED_BY_CUSTOMER:
                throw new CancelledByCustomerException();
            case INSUFFICIENT_BALANCE:
                throw new InsufficientBalanceException();
            case DUPLICATE_TRANSACTION:
                throw new TransactionDuplicationException();
        }
    }

    private void customerStatusCheck(Response response, RequestContext context){
        ResponseCode responseCode = response.getCode();
        switch (responseCode){
            case CUSTOMER_ACCOUNT_STATUS_NOT_ACTIVE:
                throw new CustomerNotActiveException();
            case CUSTOMER_PROFILE_HAS_PROBLEMS:
                throw new CustomerWithProblemsException();
        }
    }

    @Override
    public Ref submit(C2BPaymentRequest paymentRequest) throws MpesaServiceException {
        if(paymentRequest==null)
            throw new IllegalArgumentException("paymentRequest must not be null");
        //TODO: Validate if Required configurations are there
        RequestContext context = createRequestContext(paymentRequest);
        //Build and send HTTP Request
        Response response = this.buildRequest("/ipg/v1x/c2bPayment/singleStage/",context)
                .setParam("input_TransactionReference",
                        paymentRequest.getTransactionRef().toString())
                .setParam("input_CustomerMSISDN","258"+
                        paymentRequest.getMobileNumber().toString())
                .setParam("input_Amount",String.valueOf(
                        paymentRequest.getAmount().value()))
                .setParam("input_ThirdPartyReference",
                        paymentRequest.getTransactionRef().toString())
                .setParam("input_ServiceProviderCode",Configuration.current().getServiceProviderCode())
                .sendPost();//Send POST
        LOGGER.debug(response.toString());//Print Response properties
        if(response.getCode()==ResponseCode.SUCCESSFUL){
            return Ref.val(response.getParam("output_TransactionID"));
        }
        this.commonStatusCheck(response,context);
        this.customerStatusCheck(response,context);
        this.paymentStatusCheck(response,context);
        //For every response code that we don't care about
        throw new MpesaServiceException("payment failed: "+response.getCode().
                getValue());
    }

    @Override
    public Ref execute(PaymentReverse reverse) throws MpesaServiceException {
        if(reverse==null)
            throw new IllegalArgumentException("reverse must not be null");
        //TODO: Validate if Required configurations are there
        RequestContext context = createRequestContext(reverse);
        Configuration configuration = context.getConfiguration();
        //Build and send HTTP Request
        Response response = this.buildRequest("/ipg/v1x/reversal/",context)
                .setParam("input_TransactionID",
                        reverse.getTransactionRef().toString())
                .setParam("input_SecurityCredential",
                        configuration.getSecurityCredential())
                .setParam("input_InitiatorIdentifier",configuration.getInitiatorIdentifier())
                .setParam("input_ThirdPartyReference",reverse.getPaymentRef().toString())
                .setParam("input_ServiceProviderCode",configuration.getServiceProviderCode())
                .sendPut();//Send PUT
        LOGGER.debug(response.toString());//Print Response properties
        if(response.getCode()==ResponseCode.SUCCESSFUL)
            return Ref.val(response.getParam("output_TransactionID"));
        this.commonStatusCheck(response,context);
        this.customerStatusCheck(response,context);
        this.paymentStatusCheck(response,context);
        //For every response code that we don't care about
        throw new MpesaServiceException("payment reverse failed: "+response.getCode().
                getValue());
    }


    public Optional<TransactionStatus> getTransactionStatus(Ref transactionRef) throws MpesaServiceException{
        CommonValidators.requireNotNull("transactionRef",transactionRef);
        //TODO: Validate if Required configurations are there
        RequestContext context = createRequestContext(new GetTransactionStatus(transactionRef));
        //Build and send HTTP Request
        Response response = this.buildRequest("/ipg/v1x/queryTransactionStatus/",context)
                .setParam("input_ThirdPartyReference", transactionRef.toString())
                .setParam("input_QueryReference",transactionRef.toString())
                .setParam("input_ServiceProviderCode",Configuration.current().getServiceProviderCode())
                .sendGet();//Send GET
        LOGGER.debug(response.toString());//Print Response properties
        if(response.getCode()==ResponseCode.SUCCESSFUL){
            String status = response.getParam("output_ResponseTransactionStatus");
            if(status.equalsIgnoreCase("N/A"))
                return Optional.empty();
            return Optional.of(TransactionStatus.valueOf(status.toUpperCase()));
        }
        this.commonStatusCheck(response,null);
        throw new MpesaServiceException("failed to get transaction status: "+response.getCode().
                getValue());
    }


}
