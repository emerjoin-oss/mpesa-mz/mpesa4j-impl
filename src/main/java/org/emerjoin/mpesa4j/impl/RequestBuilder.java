package org.emerjoin.mpesa4j.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.NoConnectionReuseStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.emerjoin.mpesa4j.Configuration;
import org.emerjoin.mpesa4j.exception.InitializationException;
import org.emerjoin.mpesa4j.exception.service.InfrastructureException;
import org.emerjoin.mpesa4j.exception.service.MpesaUnreachableException;
import org.emerjoin.mpesa4j.exception.service.OperationTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mario Junior.
 */
public class RequestBuilder {

    private Map<String,String> params = new HashMap<>();
    private String path;
    private Configuration config;

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestBuilder.class);
    private static SSLConnectionSocketFactory SSL_TRUST_ALL_SOCKET_FACTORY;
    private static BasicHttpClientConnectionManager SSL_TRUST_ALL_CONNECTION_MANAGER;

    static {
        try {
            TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
            SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy)
                    .build();
            SSL_TRUST_ALL_SOCKET_FACTORY= new SSLConnectionSocketFactory(sslContext,
                    NoopHostnameVerifier.INSTANCE);
            Registry<ConnectionSocketFactory> socketFactoryRegistry =
                    RegistryBuilder.<ConnectionSocketFactory>create()
                            .register("https", SSL_TRUST_ALL_SOCKET_FACTORY)
                            .register("http", new PlainConnectionSocketFactory())
                            .build();
            LOGGER.debug("SSL_TRUST_ALL_SOCKET_FACTORY Ready");
            SSL_TRUST_ALL_CONNECTION_MANAGER = new BasicHttpClientConnectionManager(
                    socketFactoryRegistry);
            LOGGER.debug("SSL_TRUST_ALL_CONNECTION_MANAGER Ready");
        }catch (KeyStoreException | NoSuchAlgorithmException | KeyManagementException ex){
            throw new InitializationException("error initializing SSL_TRUST_ALL_CONNECTION_MANAGER",
                    ex);
        }
    }

    RequestBuilder(String path, Configuration configuration){
        this.path = path;
        this.config = configuration;
    }

    RequestBuilder setParam(String name, String value){
        if(name==null||name.isEmpty())
            throw new IllegalArgumentException("name must not be null nor empty");
        if(value==null||value.isEmpty())
            throw new IllegalArgumentException("value must not be null nor empty");
        this.params.put(name,value);
        return this;
    }

    private String makeRequestUrl(){
        return String.format("%s://%s:%d%s",
                (config.isSsl() ? "https" : "http"),
                config.getHostname(), config.getPort(),
                path);
    }

    private String makeParamsQueryString(){
        StringBuilder queryString = new StringBuilder();
        queryString.append("?");
        boolean first = true;
        for(String key: params.keySet()){
            if(!first)
                queryString.append("&");
            queryString.append(key);
            queryString.append("=");
            queryString.append(params.get(key));
            first = false;
        }
        return queryString.toString();
    }


    private void compose(HttpRequestBase request) throws UnsupportedEncodingException {
        if(request instanceof HttpEntityEnclosingRequestBase){
            HttpEntityEnclosingRequestBase httpEntityRequest =
                    (HttpEntityEnclosingRequestBase) request;
            httpEntityRequest.setEntity(new StringEntity(paramsToJSON()));
        }
        this.setHeaders(request);
    }

    private void setHeaders(HttpRequestBase request){
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", Constants.APPLICATION_JSON_MIME);
        request.setHeader("Origin", "developer.mpesa.vm.co.mz");
        request.setHeader("Authorization", "Bearer " + config.getToken());
    }

    private String paramsToJSON(){
        ObjectMapper objectMapper = new ObjectMapper();
        if(params.isEmpty())
            return "{}";
        try {
            return objectMapper.writeValueAsString(
                    params);
        }catch (JsonProcessingException ex){
            throw new InfrastructureException("error found serializing params map",
                    ex);
        }
    }

    public Response sendPut(){
        CloseableHttpClient client = getHttpClient();
        String url = makeRequestUrl();
        LOGGER.debug("Request URL: "+url);
        HttpPut httpPut = new HttpPut(url);
        try {
            this.compose(httpPut);
            LOGGER.debug("Sending HTTP PUT Request...");
            return Response.parse(client.execute(httpPut));
        }catch (SocketTimeoutException ex) {
            LOGGER.trace("socket timed out", ex);
            throw new OperationTimeoutException();
        }catch (UnknownHostException ex){
            throw new MpesaUnreachableException();
        }catch (IOException ex) {
            throw new InfrastructureException("error sending HTTP Request to M-Pesa API",
                    ex);
        }finally {
            try {
                LOGGER.trace("Closing HTTP Client");
                client.close();
            }catch (Exception ex){
                LOGGER.trace("Error closing HTTP Client",ex);
            }
        }
    }


    public Response sendPost(){
        CloseableHttpClient client = getHttpClient();
        String url = makeRequestUrl();
        LOGGER.debug("Request URL: "+url);
        HttpPost httpPost = new HttpPost(url);
        try {
            this.compose(httpPost);
            LOGGER.debug("Sending HTTP POST Request...");
            return Response.parse(client.execute(httpPost));
        }catch (SocketTimeoutException ex) {
            LOGGER.trace("socket timed out", ex);
            throw new OperationTimeoutException();
        }catch (UnknownHostException ex){
            throw new MpesaUnreachableException();
        }catch (IOException ex) {
            throw new InfrastructureException("error sending HTTP Request to M-Pesa API",
                    ex);
        }finally {
            try {
                LOGGER.trace("Closing HTTP Client");
                client.close();
            }catch (Exception ex){
                LOGGER.trace("Error closing HTTP Client",ex);
            }
        }
    }

    public Response sendGet(){
        CloseableHttpClient client = getHttpClient();
        String url = makeRequestUrl()+makeParamsQueryString();
        LOGGER.debug("Request URL: "+url);
        HttpGet httpGet = new HttpGet(url);
        try {
            this.compose(httpGet);
            LOGGER.debug("sending HTTP GET Request...");
            return Response.parse(client.execute(httpGet));
        }catch (HttpHostConnectException ex){
            LOGGER.trace("failed to connect to http host",ex);
            throw new MpesaUnreachableException();
        }catch (SocketTimeoutException ex) {
            LOGGER.trace("socket timed out", ex);
            throw new OperationTimeoutException();
        }catch (UnknownHostException ex){
                throw new MpesaUnreachableException();
        }catch (IOException ex){
            throw new InfrastructureException("error sending HTTP Request to M-Pesa API",
                    ex);
        }finally {
            try {
                LOGGER.trace("Closing HTTP Client");
                client.close();
            }catch (Exception ex){
                LOGGER.trace("Error closing HTTP Client",ex);
            }
        }
    }

    private CloseableHttpClient getHttpClient(){
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(config.getMaxResponseWait())
                .setConnectionRequestTimeout(config.getMaxResponseWait())
                .setSocketTimeout(config.getMaxResponseWait())
                .build();

        HttpClientBuilder clientBuilder = HttpClients.custom();
        clientBuilder.setDefaultRequestConfig(requestConfig)
                .setConnectionManagerShared(true)
                .setUserAgent("Emerjoin-Mpesa4j")
                .setConnectionReuseStrategy(new NoConnectionReuseStrategy())
                .build();

        if(config.isSsl()&&config.isSslTrustAll()){
            clientBuilder.setSSLSocketFactory(
                    SSL_TRUST_ALL_SOCKET_FACTORY).setConnectionManager(
                    SSL_TRUST_ALL_CONNECTION_MANAGER);
        }

        config.getWebProxy().ifPresent(webProxy -> {
            clientBuilder.setProxy(new HttpHost(webProxy.getHostname(), webProxy.
                    getPort()));
        });

        return clientBuilder.build();
    }

}
